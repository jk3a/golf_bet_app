# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170522201305) do

  create_table "bets", force: :cascade do |t|
    t.string   "player1a"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "player1b"
    t.string   "player2a"
    t.string   "player2b"
    t.string   "team1_front"
    t.string   "team1_back"
    t.string   "team2_front"
    t.string   "team2_back"
    t.string   "team1_result"
    t.string   "team2_result"
  end

  create_table "rounds", force: :cascade do |t|
    t.string   "name"
    t.string   "hole1_score"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "hole2_score"
    t.string   "hole3_score"
    t.string   "hole4_score"
    t.string   "hole5_score"
    t.string   "hole6_score"
    t.string   "hole7_score"
    t.string   "hole8_score"
    t.string   "hole9_score"
    t.string   "hole10_score"
    t.string   "hole11_score"
    t.string   "hole12_score"
    t.string   "hole13_score"
    t.string   "hole14_score"
    t.string   "hole15_score"
    t.string   "hole16_score"
    t.string   "hole17_score"
    t.string   "hole18_score"
  end

end
