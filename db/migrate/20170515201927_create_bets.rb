class CreateBets < ActiveRecord::Migration[5.0]
  def change
    create_table :bets do |t|
      t.string :player1a

      t.timestamps
    end
  end
end
