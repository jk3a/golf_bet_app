class AddDetailedResultsToBets < ActiveRecord::Migration[5.0]
  def change
    add_column :bets, :team1_front, :string
    add_column :bets, :team1_back, :string
    add_column :bets, :team2_front, :string
    add_column :bets, :team2_back, :string
    add_column :bets, :team1_result, :integer
    add_column :bets, :team2_result, :integer
  end
end
