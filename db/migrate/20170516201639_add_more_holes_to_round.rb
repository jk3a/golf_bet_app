class AddMoreHolesToRound < ActiveRecord::Migration[5.0]
  def change
    add_column :rounds, :hole2_score, :string
    add_column :rounds, :hole3_score, :string
  end
end
