class RemoveResultFromBets < ActiveRecord::Migration[5.0]
  def change
    remove_column :bets, :result, :string
  end
end
