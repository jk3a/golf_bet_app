class AddAllHolesToRound < ActiveRecord::Migration[5.0]
  def change
    add_column :rounds, :hole4_score, :string
    add_column :rounds, :hole5_score, :string
    add_column :rounds, :hole6_score, :string
    add_column :rounds, :hole7_score, :string
    add_column :rounds, :hole8_score, :string
    add_column :rounds, :hole9_score, :string
    add_column :rounds, :hole10_score, :string
    add_column :rounds, :hole11_score, :string
    add_column :rounds, :hole12_score, :string
    add_column :rounds, :hole13_score, :string
    add_column :rounds, :hole14_score, :string
    add_column :rounds, :hole15_score, :string
    add_column :rounds, :hole16_score, :string
    add_column :rounds, :hole17_score, :string
    add_column :rounds, :hole18_score, :string
  end
end
