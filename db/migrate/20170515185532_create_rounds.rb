class CreateRounds < ActiveRecord::Migration[5.0]
  def change
    create_table :rounds do |t|
      t.string :name
      t.string :hole1_score

      t.timestamps
    end
  end
end
