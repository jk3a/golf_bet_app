class ChangeDatatypeForResultsInBets < ActiveRecord::Migration[5.0]
  def change
    change_column :bets, :team1_result, :string
    change_column :bets, :team2_result, :string
  end
end
