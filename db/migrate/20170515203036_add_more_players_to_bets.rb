class AddMorePlayersToBets < ActiveRecord::Migration[5.0]
  def change
    add_column :bets, :player1b, :string
    add_column :bets, :player2a, :string
    add_column :bets, :player2b, :string
  end
end
