require 'test_helper'

class CalculateBetTest < ActionDispatch::IntegrationTest
  def setup
    @even_par = rounds(:even_par)
    @up_down = rounds(:up_down)
    @one_up = rounds(:one_up)
    @two_up = rounds(:two_up)
    @two_up_press = rounds(:two_up_press)
    @two_bets_won = rounds(:two_bets_won)
    @two_up_loses_one_bet = rounds(:two_up_loses_one_bet)
    @two_up_loses_two_bets = rounds(:two_up_loses_two_bets)
  end
  
  test "should get even on front, even on back" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @even_par.name,
                                     player1b: @even_par.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team1-front", "Even"
    assert_select "span#team1-back", "Even"
  end
  
  test "should get 'Up & Down' on front, 'Up & Down' on back" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player2a: @even_par.name,
                                     player2b: @even_par.name,
                                     player1a: @up_down.name,
                                     player1b: @up_down.name } }
    get bets_path
    assert_select "span#team1-front", "Up & Down"
    assert_select "span#team1-back", "Up & Down"
    assert_select "span#team2-back", ""
    assert_select "span#team2-front", ""
  end
  
  test "should get '1up' on front team1, '1up' on back team2" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @one_up.name,
                                     player1b: @one_up.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team1-front", "1up"
    assert_select "span#team2-back", "1up"
  end
  
  test "should get '2up' on front team2, '2up' on back team1" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @two_up.name,
                                     player1b: @two_up.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team2-front", "2up"
    assert_select "span#team1-back", "2up"
    assert_select "span#team1-result", ""
    assert_select "span#team2-result", ""
  end
  
  test "(with press) should get '2up' on front team1, '2up' on back team1" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @two_up_press.name,
                                     player1b: @two_up_press.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team1-front", "2up"
    assert_select "span#team1-back", "2up"
    assert_select "span#team1-result", "$2"
  end
  
  test "team1 wins '3up & 1up' front, team2 wins '4up & 2up' on back" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @two_bets_won.name,
                                     player1b: @two_bets_won.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team1-front", "3up & 1up"
    assert_select "span#team2-back", "4up & 2up"
    assert_select "span#team1-result", ""
  end
  
  test "(early winner loses)team2 wins '2up' front, team1 wins '2up' on back" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @two_up_loses_one_bet.name,
                                     player1b: @two_up_loses_one_bet.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team2-front", "2up"
    assert_select "span#team1-back", "2up"
    assert_select "span#team2-result", ""
  end
  
  test "(early winner loses)team2 wins '1up & 3up' front, team1 wins '1up & 3up' on back" do
    get new_bet_path
    assert_template 'bets/new'
    post bets_path, params: { bet: { player1a: @two_up_loses_two_bets.name,
                                     player1b: @two_up_loses_two_bets.name,
                                     player2a: @even_par.name,
                                     player2b: @even_par.name } }
    get bets_path
    assert_select "span#team2-front", "1up & 3up"
    assert_select "span#team1-back", "1up & 3up"
    assert_select "span#team2-result", ""
  end
end
