require 'test_helper'

class RoundsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get new_round_url
    assert_response :success
  end

  test "should get index" do
    get rounds_url
    assert_response :success
  end

end
