require 'test_helper'

class BetsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get bets_url
    assert_response :success
  end

  test "should get new" do
    get new_bet_url
    assert_response :success
  end

end
