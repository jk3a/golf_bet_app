# README

This Golf Betting App is designed to automate the bet calculation process of
a four-ball match in which each 9 holes has two possible bets.  The 2nd bet,
i.e. the "press" for each 9 is only triggered if one team gets two up before last
hole of the nine.
