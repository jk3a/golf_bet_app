
  function incrementValue(field)
{
    var value = parseInt(document.getElementById(field).value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById(field).value = value;
}

function decrementValue(field)
{
    var value = parseInt(document.getElementById(field).value, 10);
    value = isNaN(value) ? 0 : value;
    value--;
    document.getElementById(field).value = value;
}
