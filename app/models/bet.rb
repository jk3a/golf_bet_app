class Bet < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  
  def calculate
    #grab all scores
    player1a_scores = find_scores("1a")
    player1b_scores = find_scores("1b")
    player2a_scores = find_scores("2a")
    player2b_scores = find_scores("2b")
    
    #combine team scores into one score
    team1score = calc_team_score(player1a_scores, player1b_scores)
    team2score = calc_team_score(player2a_scores, player2b_scores)
    
    #compare two team scores
    front_outcome = determine_winner_front(team1score, team2score)
    back_outcome = determine_winner_back(team1score, team2score)
    team1_result = 0
    team2_result = 0

#----FRONT    
    #check for ties first
    if front_outcome[0] == 0 && front_outcome[1] == 0
      update_attribute(:team1_front, "Even") 
    elsif front_outcome[0].abs == 1 && front_outcome[1].abs == 1
      update_attribute(:team1_front, "Up & Down") 
    
    #check for 1up and 2up outcomes
    elsif (front_outcome[0] == 1 || front_outcome[0] == 2) && front_outcome[1] == 0
      team1_result += 1; team2_result -= 1
      update_attribute(:team1_front, "#{front_outcome[0]}up")
      
    elsif front_outcome[1] == 2 && front_outcome[0] == 0
      team1_result += 1; team2_result -= 1
      update_attribute(:team1_front, "2up")   
      
    elsif (front_outcome[0] == -1 || front_outcome[0] == -2) && front_outcome[1] == 0
      team2_result += 1; team1_result -= 1
      update_attribute(:team2_front, "#{-front_outcome[0]}up")
      
    elsif front_outcome[1] == -2 && front_outcome[0] == 0
      team2_result += 1; team1_result -= 1  
      update_attribute(:team2_front, "2up")
                  
    #2 bets won outcomes
    elsif front_outcome.reduce(:+) > 2
      team1_result += 2; team2_result -= 2
      update_attribute(:team1_front, "#{front_outcome[0]}up & #{front_outcome[1]}up")
    elsif front_outcome.reduce(:+) < -2
      team2_result += 2; team1_result -= 2
      update_attribute(:team2_front, "#{-front_outcome[0]}up & #{-front_outcome[1]}up")
    end
    
#----BACK 9    
    #check for ties first
    if back_outcome[0] == 0 && back_outcome[1] == 0
      update_attribute(:team1_back, "Even") 
    elsif back_outcome[0].abs == 1 && back_outcome[1].abs == 1
      update_attribute(:team1_back, "Up & Down") 
    
    #check for 1up and 2up outcomes
    elsif (back_outcome[0] == 1 || back_outcome[0] == 2) && back_outcome[1] == 0
      team1_result += 1; team2_result -= 1
      update_attribute(:team1_back, "#{back_outcome[0]}up") 
      
    elsif back_outcome[1] == 2 && back_outcome[0] == 0
      team1_result += 1; team2_result -= 1
      update_attribute(:team1_back, "2up")
      
    elsif (back_outcome[0] == -1 || back_outcome[0] == -2) && back_outcome[1] == 0
      team2_result += 1; team1_result -= 1
      update_attribute(:team2_back, "#{-back_outcome[0]}up")
      
    elsif back_outcome[1] == -2 && back_outcome[0] == 0
      team2_result += 1; team1_result -= 1  
      update_attribute(:team2_back, "2up")
                  
    #2 bets won outcomes
    elsif back_outcome.reduce(:+) > 2
      team1_result += 2; team2_result -= 2
      update_attribute(:team1_back, "#{back_outcome[0]}up & #{back_outcome[1]}up")
    elsif back_outcome.reduce(:+) < -2
      team2_result += 2; team1_result -= 2
      update_attribute(:team2_back, "#{-back_outcome[0]}up & #{-back_outcome[1]}up")
    end
    
    update_attribute(:team1_result, "$" + team1_result.to_s) if team1_result > 0
    update_attribute(:team2_result, "$" + team2_result.to_s) if team2_result > 0
  end  
   
  def find_scores(x)
    player_score = []
    18.times do |n|
      player_score << Round.find_by(name: self["player#{x}"])["hole#{n+1}_score"]
    end
    player_score
  end
  
  def calc_team_score(p1_scores, p2_scores)
    team_score = []
    18.times do |x|
      p1_scores[x] < p2_scores[x] ? team_score << p1_scores[x] :
                                    team_score << p2_scores[x]
    end
    team_score
  end
  
  def determine_winner_front(t1_scores, t2_scores)
    outcome = [0,0]
    press_initiated = false
    9.times do |x|
      #if the first bet ever gets 2up, trigger 2nd bet
      if outcome[0].abs == 2 && !press_initiated
        press_initiated = true
        outcome[1] = determine_winner_press(t1_scores[x...9], t2_scores[x...9])
      end
      if t1_scores[x] < t2_scores[x]
        outcome[0] += 1
      elsif t2_scores[x] < t1_scores[x]
        outcome[0] -= 1
      end
    end
    outcome
  end
  
  def determine_winner_back(t1_scores, t2_scores)
    outcome = [0,0]
    press_initiated = false
    9.times do |x|
      #if the first bet ever gets 2up, trigger 2nd bet
      if outcome[0].abs == 2 && !press_initiated
        press_initiated = true
        outcome[1] = determine_winner_press(t1_scores[x+9...18], t2_scores[x+9...18])
      end
      if t1_scores[x+9] < t2_scores[x+9]
        outcome[0] += 1
      elsif t2_scores[x+9] < t1_scores[x+9]
        outcome[0] -= 1
      end
    end
    outcome
  end
  
  def determine_winner_press(t1_scores, t2_scores)
    holes_left = t1_scores.length
    outcome = 0
    holes_left.times do |x|
      if t1_scores[x] < t2_scores[x]
        outcome += 1
      elsif t2_scores[x] < t1_scores[x]
        outcome -= 1
      end
    end
    outcome
  end
end  
  

