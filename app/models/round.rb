class Round < ApplicationRecord
  validates :name, presence: true, length: { minimum: 2, maximum: 15 }
  validates :hole1_score, presence: true, numericality: { only_integer: true }
  validates :hole2_score, presence: true, numericality: { only_integer: true }
  validates :hole3_score, presence: true, numericality: { only_integer: true }
  validates :hole4_score, presence: true, numericality: { only_integer: true }
  validates :hole5_score, presence: true, numericality: { only_integer: true }
  validates :hole6_score, presence: true, numericality: { only_integer: true }
  validates :hole7_score, presence: true, numericality: { only_integer: true }
  validates :hole8_score, presence: true, numericality: { only_integer: true }
  validates :hole9_score, presence: true, numericality: { only_integer: true }
  validates :hole10_score, presence: true, numericality: { only_integer: true }
  validates :hole11_score, presence: true, numericality: { only_integer: true }
  validates :hole12_score, presence: true, numericality: { only_integer: true }
  validates :hole13_score, presence: true, numericality: { only_integer: true }
  validates :hole14_score, presence: true, numericality: { only_integer: true }
  validates :hole15_score, presence: true, numericality: { only_integer: true }
  validates :hole16_score, presence: true, numericality: { only_integer: true }
  validates :hole17_score, presence: true, numericality: { only_integer: true }
  validates :hole18_score, presence: true, numericality: { only_integer: true }
  default_scope -> { order(created_at: :desc) }
end
