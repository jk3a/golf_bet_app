class BetsController < ApplicationController
  def index
    @bets = Bet.where("created_at >= ?", Time.zone.now.beginning_of_day)
  
  end

  def new
    @bet = Bet.new
  end
  
  def create
    @bet = Bet.create(bet_params) # will be Bet.new then calculate result before saving
    @bet.calculate
    redirect_to '/bets'
  end
  
  private
    def bet_params
      params.require(:bet).permit(:player1a, :player1b, :player2a, :player2b) 
    end
end
