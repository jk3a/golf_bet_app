class RoundsController < ApplicationController
  
  def index
    @rounds = Round.where("created_at >= ?", Time.zone.now.beginning_of_day)
    
  end
  
  def new
    @round = Round.new
  end
  
  def create
    @round = Round.new(round_params)
    if @round.save
      redirect_to rounds_url
    else
      flash[:danger] = "Hey Mel, Don't Forget Your Name!"
      redirect_to new_round_path
    end
  end
  
  def edit
    @round = Round.find(params[:id])
  end
  
  def update
    @round = Round.find(params[:id])
    if @round.update_attributes(round_params)
      flash[:success] = "Round updated"
      redirect_to rounds_path
    else
      render 'edit'
    end
  end
  
  private
    def round_params
      params.require(:round).permit(:name, :hole1_score, :hole2_score, :hole3_score, :hole4_score, :hole5_score, :hole6_score,
                                    :hole7_score, :hole8_score, :hole9_score, :hole10_score, :hole11_score, :hole12_score,
                                    :hole13_score, :hole14_score, :hole15_score, :hole16_score, :hole17_score, :hole18_score)
    end
end
