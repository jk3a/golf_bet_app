Rails.application.routes.draw do
  root 'static_pages#home'
  resources :rounds
  resources :bets
end
